stages:
  - version
  - build
  - deploy

version:
  image:
    name: gittools/gitversion
    entrypoint: ['']
  stage: version
  variables:
    GIT_STRATEGY: none
  script:
    - |
      if [[ "${CI_SCRIPT_TRACE}" == "true" ]] || [[ -n "${CI_DEBUG_TRACE}" ]]; then
        echo "Debugging enabled"
        set -xv
      fi
      # Checkout the GitVersion.yml if one exists - we'll use partial clone
      git clone --filter=blob:none --no-checkout $CI_REPOSITORY_URL $CI_PROJECT_DIR
      git checkout $CI_COMMIT_SHA GitVersion.yml || true

      if [[ "${CI_GITVERSION_TRACE}" == "true" ]] || [[ -n "${CI_DEBUG_TRACE}" ]]; then
        echo "Tracing is on, showing Gitversion configuration (including settings from defaults)..."
        /tools/dotnet-gitversion /url $CI_REPOSITORY_URL /u gitlab-ci-token /p $CI_JOB_TOKEN /b $CI_COMMIT_REF_NAME /c $CI_COMMIT_SHA /dynamicRepoLocation $CI_PROJECT_DIR /showconfig
        VERBOSITYSETTING='/verbosity Diagnostic'
      fi
      if [[ "$GIT_STRATEGY" != 'none' ]]; then
        echo "GitLab CI's default cloning strategy is highly optimized and not compatible with Gitversion, please set `variables: GIT_STRATEGY: none` just for this job."
        echo "gitversion's 'Dynamic Repositories' feature is used to clone the entire repository."
      fi
      echo "running git version and setting version in pipeline variables using dotenv artifacts"
      /tools/dotnet-gitversion /url $CI_REPOSITORY_URL /u gitlab-ci-token /p $CI_JOB_TOKEN /b $CI_COMMIT_REF_NAME /c $CI_COMMIT_SHA /dynamicRepoLocation $CI_PROJECT_DIR $VERBOSITYSETTING | tee thisversion.json
      for keyval in $( grep -E '": [^\{]' thisversion.json | sed -e 's/: /=/' -e "s/\(\,\)$//"); do
        echo "export $keyval"
        eval export $keyval
      done
      echo "Exporting some of these to dotenv files for variable usage in the pipeline and subsequent jobs..."
      echo "SemVer=${SemVer}" >> thisversion.env
      echo "PACKAGE_VERSION=${LegacySemVer}" >> thisversion.env
      echo "LegacySemVer=${LegacySemVer}" >> thisversion.env
      echo "InformationalVersion=${InformationalVersion}" >> thisversion.env
      echo "Major=${Major}" >> thisversion.env
      echo "Minor=${Minor}" >> thisversion.env
      echo "Patch=${Patch}" >> thisversion.env
      echo "MajorMinorPatch=${MajorMinorPatch}" >> thisversion.env
      echo "NuGetVersionV2=${NuGetVersionV2}" >> thisversion.env
  artifacts:
    reports:
      #propagates variables into the pipeline level
      dotenv: thisversion.env

cake-build:
  stage: build
  tags:
    - shared-windows
    - windows
    - windows-1809
  script:
    - ./dotnet-install.ps1
    - ./build.ps1
  artifacts:
    paths:
      - Documentation/_site
      - Build/Artifacts/*.nupkg
    expire_in: 1 week

deploy-site:
  image:
    name: amazon/aws-cli
    entrypoint: [""]
  stage: deploy
  when: manual
  only:
    variables:
      - $CI_COMMIT_REF_PROTECTED == "true"
  script:
    - aws --version
    - echo "s3://curvacy.abrevinci.com/${MajorMinorPatch}/"
    - aws s3 cp "./Documentation/_site" "s3://curvacy.abrevinci.com/${MajorMinorPatch}/" --recursive

deploy-local:
  image: mcr.microsoft.com/dotnet/sdk:6.0
  stage: deploy
  when: manual
  script:
    - cd Build/Artifacts
    - echo "Deploying:"
    - ls
    - dotnet nuget add source "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/nuget/index.json" --name gitlab --username gitlab-ci-token --password $CI_JOB_TOKEN --store-password-in-clear-text
    - dotnet nuget push "AbreVinci.Curvacy.${NuGetVersionV2}.nupkg" --source gitlab

deploy-public:
  image: mcr.microsoft.com/dotnet/sdk:6.0
  stage: deploy
  only:
    - development
    - /^release/.*$/
    - /^hotfix/.*$/
    - production
  when: manual
  script:
    - cd Build/Artifacts
    - echo "Deploying:"
    - ls
    - dotnet nuget push "AbreVinci.Curvacy.${NuGetVersionV2}.nupkg" -k ${NUGET_API_KEY} -s https://api.nuget.org/v3/index.json
