#tool nuget:?package=docfx.console&version=2.59.4

#addin nuget:?package=Sprache&version=2.3.1
#addin nuget:?package=DotNetEnv&version=2.3.0

if (string.IsNullOrWhiteSpace(EnvironmentVariable("GITLAB_CI")))
{
	DotNetEnv.Env.Load();
}

var solution = "AbreVinci.Curvacy.sln";

var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");

var artifactsDir = new DirectoryPath("./Build/Artifacts");

var projectsToPack = new[]
{
	(project: "AbreVinci.Curvacy/AbreVinci.Curvacy.csproj", package: "AbreVinci.Curvacy")
};

var version = new
{
	NuGetVersionV2 = EnvironmentVariable("NuGetVersionV2"),
	MajorMinorPatch = EnvironmentVariable("MajorMinorPatch"),
	InformationalVersion = EnvironmentVariable("InformationalVersion")
};

var isProtectedBranch = EnvironmentVariable("CI_COMMIT_REF_PROTECTED", false);

Task("Inform")
.Does(() =>
{
	Information($"Building {version.NuGetVersionV2}, {version.MajorMinorPatch}, {version.InformationalVersion}");
});

Task("Clean")
.IsDependentOn("Inform")
.Does(() =>
{
	CleanDirectory("./Build");
	CleanDirectory("./Documentation/_site");
});

Task("Build")
.IsDependentOn("Clean")
.Does(() =>
{
	var settings = new DotNetBuildSettings
	{
		Configuration = configuration,
		ArgumentCustomization = args => args
			.Append($"-p:Version={version.MajorMinorPatch}")
			.Append($"-p:InformationalVersion={version.InformationalVersion}")
	};

	DotNetBuild(solution, settings);
});

Task("Test")
.IsDependentOn("Build")
.Does(() =>
{
	var settings = new DotNetTestSettings
	{
		Configuration = configuration,
		NoBuild = true
	};

	DotNetTest(solution, settings);
});

Task("Sign")
.IsDependentOn("Build")
.Does(() =>
{
	if (!isProtectedBranch)
	{
		Information("Signing can only be done on protected branches/tags.");
		return;
	}

	SignAssemblies(GetFiles("./Build/**/AbreVinci.*.dll"));
});

Task("Package")
.IsDependentOn("Sign")
.Does(() =>
{
	var settings = new DotNetPackSettings
	{
		Configuration = configuration,
		NoBuild = true,
		OutputDirectory = new DirectoryPath($"./Build/{configuration}"),
		ArgumentCustomization = args => args
			.Append($"-p:PackageVersion={version.NuGetVersionV2}")
	};

	foreach (var project in projectsToPack)
	{
		DotNetPack($"./{project.project}", settings);		
	}
});

Task("SignPackages")
.IsDependentOn("Package")
.Does(() =>
{
	if (!DirectoryExists(artifactsDir))
		CreateDirectory(artifactsDir);

	if (!isProtectedBranch)
	{
		Information("Signing can only be done on protected branches/tags.");
		foreach (var project in projectsToPack)
		{
			CopyFileToDirectory(new FilePath($"./Build/{configuration}/{project.package}.{version.NuGetVersionV2}.nupkg"), artifactsDir);
		}
		return;
	}

	foreach (var project in projectsToPack)
	{
		SignPackage(artifactsDir, new FilePath($"./Build/{configuration}/{project.package}.{version.NuGetVersionV2}.nupkg"));
	}
});

Task("BuildDocs")
.IsDependentOn("Clean")
.Does(() => 
{ 
	var docsDirectory = MakeAbsolute(new DirectoryPath("./Documentation"));
	StartProcessFailOnError("./tools/docfx.console.2.59.4/tools/docfx.exe", new ProcessSettings {Arguments="metadata --force", WorkingDirectory=docsDirectory});
	StartProcessFailOnError("./tools/docfx.console.2.59.4/tools/docfx.exe", new ProcessSettings {Arguments="build", WorkingDirectory=docsDirectory});
});

Task("Default")
.IsDependentOn("Test")
.IsDependentOn("SignPackages")
.IsDependentOn("BuildDocs")
.Does(() => { });

Information($"Running task: {target}");
RunTarget(target);

///////////////////////////////////////////////////////////////////////////////////////
// Utilities
///////////////////////////////////////////////////////////////////////////////////////

void SignAssemblies(IEnumerable<FilePath> files)
{
	var assembliesToSignFile = new FilePath("./Build/AssembliesToSign.txt");
	System.IO.File.WriteAllLines(MakeAbsolute(assembliesToSignFile).ToString(), files.Select(p => MakeAbsolute(p).ToString()).ToArray());

	var args = new ProcessArgumentBuilder();
	args.Append("sign");

	SetupSigning(args);

	args.Append("--input-file-list");
	args.AppendQuoted(MakeAbsolute(assembliesToSignFile).ToString());

	DotNetTool("azuresigntool", args);
}

void SignPackage(DirectoryPath outputPath, FilePath package)
{
	var outputPackage = outputPath.CombineWithFilePath(package.GetFilename());

	var args = new ProcessArgumentBuilder();
	args.Append("sign");

	SetupSigning(args);

	args.Append("-o");
	args.AppendQuoted(MakeAbsolute(outputPackage).ToString());

	args.AppendQuoted(MakeAbsolute(package).ToString());

	DotNetTool("nugetkeyvaultsigntool", args);
}

void SetupSigning(ProcessArgumentBuilder args)
{
	args.Append("--file-digest");
	args.Append("sha256");

	args.Append("--timestamp-rfc3161");
	args.AppendQuoted(EnvironmentVariable("DIGICERT_TIMESTAMP_URL"));

	args.Append("--timestamp-digest");
	args.Append("sha256");

	args.Append("--azure-key-vault-url");
	args.AppendQuoted(EnvironmentVariable("AZURE_KEY_VAULT_URL"));

	args.Append("--azure-key-vault-client-id");
	args.AppendQuoted(EnvironmentVariable("AZURE_KEY_VAULT_CLIENT_ID"));

	args.Append("--azure-key-vault-tenant-id");
	args.AppendQuoted(EnvironmentVariable("AZURE_KEY_VAULT_TENANT_ID"));

	args.Append("--azure-key-vault-client-secret");
	args.AppendQuoted(EnvironmentVariable("AZURE_KEY_VAULT_CLIENT_SECRET"));

	args.Append("--azure-key-vault-certificate");
	args.AppendQuoted(EnvironmentVariable("AZURE_KEY_VAULT_CERTIFICATE"));
}

void DotNetTool(string tool, ProcessArgumentBuilder args)
{
	StartProcessFailOnError("dotnet", new ProcessSettings {Arguments = $"tool run {tool} {args.Render()}"});
}

void StartProcessFailOnError(string process, ProcessSettings settings)
{
	var exitCode = StartProcess(process, settings);
	if (exitCode != 0)
	{
		Information($"Exit code: {exitCode}");
		throw new Exception($"Error executing {process} {settings.Arguments}");
	}
}
